package com.example.blogbackend.repository;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import com.example.blogbackend.Entity.Article;
import com.example.blogbackend.Entity.Comment;
import java.time.LocalDate;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/DB.sql"})
class CommentRepoTest {

	@Autowired
	CommentRepo commentRepo;

	@Test
	void save() {
		Comment comment = new Comment(
				1,
				"Louise Pinpon",
				"Lorem Ipsum",
				LocalDate.parse("2022-10-21")
		);
		commentRepo.save(comment);
		assertThat(comment.getId()).isNotNull();
	}
	@Test
	void findAll() {
		List<Comment> result = commentRepo.findAll();
		Assertions.assertThat(result)
				.hasSize(5)
				.doesNotContainNull()
				.allSatisfy(comment ->
						Assertions.assertThat(comment).hasNoNullFieldsOrProperties()
				);
	}
	@Test
	void findByArticleId() {
		List<Comment> result = commentRepo.FindByArticleId(1);
		assertThat(result)
				.hasSize(2);
	}

	@Test
	void findById() {
		Comment comment = commentRepo.findById(1);
		Assertions.assertThat(comment)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrPropertyWithValue("author", "Pauline Lemur");
	}
	@Test
	void updateById() {
		Comment comment = new Comment(
				1,
				1,
				"Michel",
				"Chats",
				LocalDate.parse("2022-06-17")
		);
		commentRepo.updateById(comment);
		comment = commentRepo.findById(1);
		assertThat(comment)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrPropertyWithValue("id_article", 1)
				.hasFieldOrPropertyWithValue("author", "Michel")
				.hasFieldOrPropertyWithValue("content", "Chats");

	}

	@Test
	void delete() {
		commentRepo.delete(1);
		Comment result = commentRepo.findById(1);
		assertThat(result).isNull();
	}
}
